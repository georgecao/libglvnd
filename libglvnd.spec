Name:           libglvnd
Version:        1.6.0
Release:        1
Epoch:          1
Summary:        The GL Vendor-Neutral Dispatch library
License:        MIT
URL:            https://github.com/NVIDIA/libglvnd
Source0:        https://github.com/NVIDIA/libglvnd/archive/v%{version}/%{name}-%{version}.tar.gz

Patch6000:      backport-libglvnd-python3.patch
Patch6001:      backport-0001-glx-Add-another-fallback-library-name.patch

BuildRequires:  libtool xorg-x11-server-Xvfb pkgconfig(xext) pkgconfig(x11)
BuildRequires:  gcc python3-rpm-macros python3-libxml2 pkgconfig(glproto) make

%description
libglvnd is a vendor-neutral dispatch layer for arbitrating OpenGL API calls between multiple vendors.
It allows multiple drivers from different vendors to coexist on the same filesystem, and determines
which vendor to dispatch each API call to at runtime.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{epoch}:%{version}-%{release}
Requires:       %{name}-opengl%{?_isa} = %{epoch}:%{version}-%{release}
Requires:       %{name}-gles%{?_isa} = %{epoch}:%{version}-%{release}
Requires:       %{name}-glx%{?_isa} = %{epoch}:%{version}-%{release}
Requires:       %{name}-egl%{?_isa} = %{epoch}:%{version}-%{release}
Requires:       %{name}-core-devel%{?_isa} = %{epoch}:%{version}-%{release}
Requires:	libX11-devel%{?_isa}
Obsoletes:	mesa-libGLES-devel < %{epoch}:%{version}-%{release}
Provides:	mesa-libGLES-devel = %{epoch}:%{version}-%{release}
Provides:	mesa-libGLES-devel%{?_isa} = %{epoch}:%{version}-%{release}
Obsoletes:	mesa-khr-devel < %{epoch}:%{version}-%{release}
Provides:	mesa-khr-devel = %{epoch}:%{version}-%{release}
Provides:	mesa-khr-devel%{?_isa} = %{epoch}:%{version}-%{release}
Provides:	libGLES-devel = %{epoch}:%{version}-%{release}
Provides:	libGLES-devel%{?_isa} = %{epoch}:%{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        core-devel
Summary:        Core development files for %{name}

%description core-devel
The %{name}-core-devel package is a bootstrap trick for Mesa, which wants
to build against the %{name} headers but does not link against any of
its libraries (and, initially, has file conflicts with them). If you are
not Mesa you almost certainly want %{name}-devel instead.

%package        opengl
Summary:        OpenGL support for libglvnd
Requires:       %{name}%{?_isa} = %{epoch}:%{version}-%{release}
 
%description    opengl
libOpenGL is the common dispatch interface for the workstation OpenGL API.

%package        gles
Summary:        GLES support for libglvnd
Requires:       %{name}%{?_isa} = %{epoch}:%{version}-%{release}
Requires:       mesa-libEGL%{?_isa} >= 13.0.4-1
Obsoletes:      mesa-libGLES < 19.3.0~rc1
Provides:       mesa-libGLES
Provides:       mesa-libGLES%{?_isa}
Provides:       libGLES
Provides:       libGLES%{?_isa}
 
%description    gles
libGLESv[12] are the common dispatch interface for the GLES API.
 
%package        egl
Summary:        EGL support for libglvnd
Requires:       %{name}%{?_isa} = %{epoch}:%{version}-%{release}
Requires:       mesa-libEGL%{?_isa} >= 13.0.4-1
Provides:       libEGL
Provides:       libEGL%{?_isa}
 
%description    egl
libEGL are the common dispatch interface for the EGL API.
 
%package        glx
Summary:        GLX support for libglvnd
Requires:       %{name}%{?_isa} = %{epoch}:%{version}-%{release}
Requires:       mesa-libGL%{?_isa} >= 13.0.4-1
Provides:       libGL
Provides:       libGL%{?_isa}
 
%description    glx
libGL and libGLX are the common dispatch interface for the GLX API.
%prep
%autosetup -n %{name}-%{version} -p1
autoreconf -vif

%build
export PYTHON=%{__python3}
%configure \
  --disable-static \
  --enable-asm \
  --enable-tls

%make_build V=1

%install
%make_install
%delete_la	

mkdir -p %{buildroot}%{_sysconfdir}/glvnd/egl_vendor.d/
mkdir -p %{buildroot}%{_datadir}/glvnd/egl_vendor.d/
mkdir -p %{buildroot}%{_sysconfdir}/egl/egl_external_platform.d/
mkdir -p %{buildroot}%{_datadir}/egl/egl_external_platform.d/

%check
export DO_X11_TESTS=1
xvfb-run -s '-screen 0 640x480x24' -d make check V=1 || (cat `find . -name test-suite.log` ; exit 1)

%ldconfig_scriptlets

%files
%defattr(-,root,root)
%doc README.md
%dir %{_sysconfdir}/glvnd/
%dir %{_datadir}/glvnd/
%{_libdir}/libGLdispatch.so.0*
%ldconfig_scriptlets opengl
%files opengl
%{_libdir}/libOpenGL.so.0*
 
%ldconfig_scriptlets gles
%files gles
%{_libdir}/libGLES*.so.*
 
%ldconfig_scriptlets glx
%files glx
%{_libdir}/libGL.so.*
%{_libdir}/libGLX.so.*
 
%ldconfig_scriptlets egl
%files egl
%dir %{_sysconfdir}/glvnd/egl_vendor.d/
%dir %{_datadir}/glvnd/egl_vendor.d/
%dir %{_sysconfdir}/egl/
%dir %{_sysconfdir}/egl/egl_external_platform.d/
%dir %{_datadir}/egl/
%dir %{_datadir}/egl/egl_external_platform.d/
%{_libdir}/libEGL*.so.*
 
%files core-devel
%dir %{_includedir}/glvnd/
%{_includedir}/glvnd/*.h
%{_libdir}/pkgconfig/libglvnd.pc

%files devel
%defattr(-,root,root)
%dir %{_includedir}/EGL/
%dir %{_includedir}/GL/
%dir %{_includedir}/GLES/
%dir %{_includedir}/GLES2/
%dir %{_includedir}/GLES3/
%dir %{_includedir}/KHR/
%{_includedir}/EGL/*.h
%{_includedir}/GL/*.h
%{_includedir}/GLES/*.h
%{_includedir}/GLES2/*.h
%{_includedir}/GLES3/*.h
%{_includedir}/KHR/*.h
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/gl*.pc
%{_libdir}/pkgconfig/egl.pc
%{_libdir}/pkgconfig/opengl.pc
 
%changelog
* Sat Jul 22 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 1:1.6.0-1
- update to 1.6.0

* Tue Oct 25 2022 wangkerong <wangkerong@h-partners.com> - 1:1.3.4-3
- rebuild for next release

* Fri Dec 17 2021 hanhui <hanhui15@huawei.com> - 1:1.3.4-2
- Remove unnecessary installation dependencies

* Wed Dec 1 2021 xingxing<xingxing9@huawei.com> - 1:1.3.4-1
- Update to 1.3.4
- split glx egl gles package

* Mon Sep 13 2021 wangkerong <wangkerong@huawei.com> - 1:1.3.2-3
- split opengl core-devel subpackages from devel package

* Mon Aug 03 2020 orange-snn <songnannan2@huawei.cpm> - 1:1.3.2-2
- add mesa-libGLES-devel and mesa-khr-devel in provides

* Wed Jul 29 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:1.3.2-1
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:upgrade to 1.3.2

* Tue Jan 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:1.2.0-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:delete the arch in obsoletes

* Tue Jan 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:1.2.0-2
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:optimization the spec

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:1.2.0-1
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:updtae to 1.2.0

* Tue Oct 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:1.1.0-4
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the provides of libglvnd-core-devel

* Tue Sep 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:1.1.0-3
- Package init
